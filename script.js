/*Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:
У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку 
відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна 
бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки
можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті,
через такі правки не переставала працювати.
*/

const tabsButton = Array.from(document.querySelector(".tabs").children)
const tabscontent = document.querySelectorAll(".tabs-content>li")

 tabscontent[0].classList.add("item")

tabsButton.forEach((tabElement, i) => {
    tabElement.addEventListener("click", e => {       
        tabsButton.forEach(tabElement => {
            tabElement.classList.remove("active")                       
        })
        tabscontent.forEach(element => {
            element.classList.remove("item") 
        })
        e.target.classList.add("active")        
        tabscontent[i].classList.add("item")
    })
})
